#!/usr/bin/env python

import os
import sys
from setuptools import setup, find_packages

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
doclink = """
Documentation
-------------

The full documentation is at http://beattracker.rtfd.org."""
history = open('HISTORY.rst').read().replace('.. :changelog:', '')


name = 'beattracker'
author = 'Magnus "Loxosceles" Henkel'
version = '0.1'
release = '0.1.0'
setup(
    name=name,
    author=author,
    author_email='loxosceles@gmx.de',
    version=release,
    description='Beat-tracking framework with Neural Networks',
    long_description=readme + '\n\n' + doclink + '\n\n' + history,
    url='https://gitlab.com/loxosceles/beattracker',
    packages=find_packages(),
    package_dir={'beattracker': 'beattracker'},
    include_package_data=True,
        # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=["docutils>=0.3"],
    license='MIT',
    zip_safe=False,
    keywords='beattracker',
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', release),
            'source_dir': ('setup.py', 'doc')}},
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
)
