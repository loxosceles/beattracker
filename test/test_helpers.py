#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for helpers."""

import pytest
from os import path

from beattracker.util.helpers import remove_files, silent_create


def test_silent_create_with_non_existing_directory(tmpdir):
    td = tmpdir.mkdir('test_dir')
    silent_create(td)
    assert path.isdir(td) is True


def test_silent_create_with_existing_directory():
    silent_create('/tmp/testdir')
    assert path.isdir('/tmp/testdir') is True


def test_remove_files_in_dir_tree(test_dir, file):
    remove_files(test_dir)
    assert path.isfile(file) is False


@pytest.mark.parametrize("test_input,expected", [
    ('test.json', False),
    ('test.csv', False),
    ('test.hdf5', False),
    ('test.log', False),
    ('test.pickle', True),
])
def test_remove_files_targets_correct_files(test_input, expected, test_dir):
    tmpfile = test_dir.join(test_input)
    tmpfile.write("content")
    remove_files(test_dir)
    assert path.isfile(tmpfile) is expected


def test_remove_files_does_not_remove_other_files(test_dir):
    tmpfile = test_dir.join('test.txt')
    tmpfile.write("content")
    remove_files(test_dir)
    #  remove_files(test_dir, False)
    assert path.isfile(tmpfile) is True
