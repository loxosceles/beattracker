#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest


@pytest.mark.parametrize("test_input, expected", [
    (29, (14, 14)),
    (30, (15, 15)),
    (31, (15, 15)),
])
def test_divide_chunk_size(data_gen, test_input, expected):
    assert data_gen.divide_chunk_size(test_input) == expected


def test_determine_hotpatch_boundaries(data_gen):
    data_gen.c_padding = 3
    data_gen.c_chunk_size = 30
    res = data_gen.determine_hotpatch_boundaries(115, 15, 15, 3)
    assert res == (103, 127)


def test_get_list_ids_batch(data_gen, cfg):
    batch = data_gen.get_list_ids_batch(0)
    asc_list = list(x for x in range(batch[0], batch[0] +
                                     cfg.training_params.batch_size))
    assert batch == asc_list


@pytest.mark.parametrize("params, expected", [
    ((0, 100), False),
    ((1000, 1100), False),
    ((100, 1000), True),
    ((100, 200), True),
    ((111, 3000), True),
])
def test_has_onset(data_gen, params, expected):
    onset_frames = list(range(100, 1000))
    lower = params[0]
    upper = params[1]
    assert data_gen.has_onset(onset_frames, lower, upper) == expected
