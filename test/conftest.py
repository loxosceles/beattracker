#!/usr/bin/env python3

import pytest

from os import path, listdir
import json
import dill
from pathlib import Path
from dot_configs.dot_configs import Configurations
from beattracker.util.conf_helpers import build_path_object
from beattracker.util.helpers import unpickle_file
from beattracker.networks.cnn import CNN
from beattracker.util.file_object import FileObject

TEST_DIR = path.dirname(__file__)


def fill_in_placeholders(_dict, placeholder, replacement):
    return json.loads(json.dumps(_dict).replace("{{" + placeholder + "}}", replacement))


def pytest_configure():
    # Dirs
    pytest.DATA_DIR = path.join(TEST_DIR, 'data')
    pytest.AUDIO_DIR = path.join(pytest.DATA_DIR, 'audio')
    pytest.ANNOT_DIR = path.join(pytest.DATA_DIR, 'annotations')
    pytest.GENERATORS_DIR = path.join(pytest.DATA_DIR, 'data_generators')
    pytest.BATCH_DIR = path.join(pytest.DATA_DIR, 'batch')
    # Files
    pytest.STATIC_CONFIGS = path.join(pytest.DATA_DIR, 'conf/conf.json')
    pytest.CNN_JSON = Path(pytest.DATA_DIR, 'json/cnn.json')


@pytest.fixture(scope="session")
def st_cfg():
    with open(pytest.STATIC_CONFIGS, 'r') as infile:
        st_configs = json.load(infile)

    st_configs = fill_in_placeholders(
        st_configs, "TEST_DATA_DIR", pytest.DATA_DIR)

    return Configurations(st_configs).get_configuration()


@pytest.fixture(scope="session")
def cfg():
    return Configurations(pytest.CNN_JSON).get_configuration()


@pytest.fixture(scope="session")
def paths(cfg, st_cfg):
    return build_path_object(cfg, cfg.args, st_cfg)


@pytest.fixture(scope="session")
def file_list():
    return [path.join(pytest.AUDIO_DIR, f) for f in listdir(pytest.AUDIO_DIR)]


@pytest.fixture(scope="session")
def audio_path(file_list):
    return path.join(pytest.AUDIO_DIR, 'blues.00001.au')


@pytest.fixture(scope="session")
def annot_path(file_list):
    return path.join(pytest.ANNOT_DIR, 'blues.00001.txt')


@pytest.fixture(scope="session")
def pickle_index(paths):
    return paths.files.pickle_index


@pytest.fixture(scope="session")
def index_dict(paths, pickle_index):
    index = unpickle_file(paths.dirs.pickle, pickle_index)
    return index


@pytest.fixture(scope="session")
def fobj(audio_path, annot_path, cfg):
    return FileObject(audio_path, annot_path, cfg.audio_params.hop_size,
                      cfg.audio_params.num_bands, cfg.audio_params.frame_sizes)


@pytest.fixture(scope="session")
def onset_list():
    return [0.015705, 0.915349, 1.797762, 2.66851, 3.563149, 4.441049, 5.305687, 6.139932,
            7.007328, 7.895491, 8.77785, 9.660208, 10.548371]


@pytest.fixture(scope="session")
def filename():
    return path.join(pytest.DATA_DIR, 'audio/blues.00001.au')


@pytest.fixture(scope="session")
def annotation():
    return path.join(pytest.DATA_DIR, 'annotations')


@pytest.fixture(scope="function")
def data_gen():
    train_gen_path = path.join(pytest.DATA_DIR, 'data_generators/training_gen')
    with open(train_gen_path, 'rb') as infile:
        gen = dill.load(infile)
    return gen


@pytest.fixture(scope="function")
def batch():
    batch = unpickle_file(
        pytest.BATCH_DIR, 'train_gen_32_8th.pickle')
    return batch


@pytest.fixture(scope="function")
def fake_model(batch):
    class Model:
        def predict_on_batch(self, dummy):
            return batch
    m = Model()
    return m


@pytest.fixture(scope="session")
def model_cnn(paths):
    input_shape = (10, 40, 1)
    nn = CNN(input_shape, paths, cfg.architecture.cnn)
    model = nn.build()
    nn.compile(model)
    return model


@pytest.fixture(scope="function")
def test_dir(tmpdir):
    return tmpdir.mkdir('test_dir')


@pytest.fixture(scope="function")
def file(test_dir):
    tmpfile = test_dir.join('test.json')
    tmpfile.write("content")
    return tmpfile
