#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest


@pytest.mark.parametrize("param, expected", [
    ((0.000000, 44100), 0),
    ((1.000000, 44100), 44100),
    ((0.015705, 44100), 693),
])
def test_time_to_sample(param, expected, fobj):
    t = param[0]
    fobj.sample_rate = param[1]
    assert fobj.time_to_sample(t) == expected


@pytest.mark.parametrize("param, expected", [
    (1.2, (1, 0.2)),
    (1.201, (1, 0.201)),
    (1.2000, (1, 0.2000)),
    (11.09, (11, 0.09)),
])
def test_divide_float(param, expected, fobj):
    assert fobj.divide_float(param) == expected
