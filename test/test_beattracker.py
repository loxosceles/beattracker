#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  import pytest
#  from beattracker.trainer.train import filter_invalid_indices

#  @pytest.mark.parametrize("test_input, chunk_size, expected", [
#      (10, 20, 10),  # both are first file, likewise index 25
#      (3996, 6, 3996),
#  ])
#  def test_filter_invalid_indices_valid_inputs(test_input, chunk_size,
#                                                        expected, index_dict):
#      index_dict = filter_invalid_indices(index_dict, chunk_size)
#      file_name = index_dict[25][0]
#      assert index_dict[test_input] == (file_name, expected)
#
#  @pytest.mark.parametrize("test_input, chunk_size", [
#      (9, 20),
#      (2, 6),
#      (3997, 6),
#  ])
#  def test_filter_invalid_indices_invalid_inputs(test_input, chunk_size,
#                                                        index_dict):
#      index_dict = filter_invalid_indices(index_dict, chunk_size)
#      with pytest.raises(KeyError):
#          index_dict[test_input]
#
#  def tests_filter_invalid_indices_with_artificial_dict():
#      d = {k: ('dummy', k) for k in range(100)}
#      dd = filter_invalid_indices(d, 4)
#      assert list(dd.keys()) == [x for x in range(2, 98)]
