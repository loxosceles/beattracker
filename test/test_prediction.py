#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest
import numpy as np

from beattracker.postprocessing.prediction import (slice_prediction,
                                                   apply_threshold)


@pytest.fixture
def pred_matrix():
    return np.array([[0.34157466, 0.17862935],
                     [0.07164812, 0.56656797],
                     [0.6028631, 0.9742472],
                     [0.76184322, 0.37482681],
                     [0.08894479, 0.50020931]])


@pytest.fixture
def pred_matrix_beat_pos():
    return np.array([0.17862935,
                     0.56656797,
                     0.9742472,
                     0.37482681,
                     0.50020931])


def test_slice_prediction_matrix(pred_matrix, pred_matrix_beat_pos):
    sliced_matrix = slice_prediction(pred_matrix)
    assert np.array_equal(sliced_matrix, pred_matrix_beat_pos)


def test_apply_threshold_to_prediction_matrix(pred_matrix_beat_pos):
    assert np.array_equal(apply_threshold(pred_matrix_beat_pos, 0.5),
                          np.array([0.0, 1.0, 1.0, 0.0, 1.0]))
