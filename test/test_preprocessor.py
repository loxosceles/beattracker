#!/usr/bin/env python3
# -*- coding: utf-8 -*=

"""Tests for `preprocessor`."""

import pytest
from os import path
from beattracker.util.preprocessor import (compose_annotation_path,
                                           extract_file_pattern,
                                           calculate_limit,
                                           update_mapping,
                                           generate_limited_mappings)


@pytest.mark.parametrize("param, expected", [
    ('/test/path/audio/blues.00001.au', 'blues.00001'),
    ('/test/path/audio/blues-jazz.00001.wav', 'blues-jazz.00001'),
    ('/test/path/audio/blues.wav', 'blues'),
    ('../audio/blues.00001.au', 'blues.00001')
])
def test_extract_file_pattern(param, expected, fobj):
    fobj.audio_file_path = param
    assert extract_file_pattern(param) == expected


def test_update_mapping():
    mapping = {0: ("path/to/file", 0), 1: ("path/to/file", 1)}
    update_mapping(mapping, 2, 'path/to/new/file')
    assert mapping == {0: ("path/to/file", 0),
                       1: ("path/to/file", 1),
                       2: ("path/to/new/file", 0),
                       3: ("path/to/new/file", 1)
                       }


@pytest.mark.parametrize("param, expected", [
    ('blues.00001', 'blues.00001.txt'),
    ('blues-jazz.00001', 'blues-jazz.00001.txt'),
    ('blues.00001', 'blues.00001.txt'),
])
def test_extract_annotation_path(param, expected, fobj, paths):
    assert compose_annotation_path(param, paths.dirs.annotations) == path.join(
        paths.dirs.annotations, expected)


def test_index_dict_starts_at_zero_key_and_value(index_dict):
    assert list(index_dict.keys())[0] == index_dict[0][1]


def test_index_dict_counts_consecutively(index_dict):
    assert list(index_dict.keys()) == list(range(0, len(index_dict.keys())))


@pytest.mark.parametrize("param, expected", [
    ((12000, 0.5), 6000),
    ((12001, 0.5), 6000),
    ((12002, 0.5), 6001),
])
def test_calculate_dataset(param, expected):
    number_of_files = param[0]
    percentage = param[1]
    assert calculate_limit(number_of_files, percentage) == expected


@pytest.mark.parametrize("param, expected", [
    ((12000, 1), 12000),
    ((12002, -1.5), 6001),
    ((12002, -1.5), 6001),
])
def test_calculate_limit_raises_value_error_when_percentage_incorrect(param,
                                                                      expected):
    number_of_files = param[0]
    percentage = param[1]
    with pytest.raises(ValueError, match=r".*Percentage .*"):
        assert calculate_limit(number_of_files, percentage) == expected


@pytest.mark.parametrize("param, expected", [
    (1, 3),
    (2, 4),
    (3, 5),
    (4, 7)
])
def test_generate_limited_mappings(param, expected):
    mapping = {0: ("path/to/file1", 0),
               1: ("path/to/file1", 1),
               2: ("path/to/file1", 2),
               3: ("path/to/file2", 0),
               4: ("path/to/file3", 0),
               5: ("path/to/file4", 0),
               6: ("path/to/file4", 1)
               }
    assert len(generate_limited_mappings(mapping, param)) == expected
