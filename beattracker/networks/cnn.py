#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from keras import layers
from keras.models import Sequential

from beattracker.networks.nn import NN


class CNN(NN):
    """CNN class."""

    def __init__(self, version, input_shape, paths, training_params, layers):
        print("Building CNN")
        self.name = 'Convolutional Neural Network'
        self.nn_architecture = 'CNN'
        self.version = version
        self.optimizer = NN._optimizers[training_params.optimizer]
        self.losses = NN._losses[training_params.losses]
        self.metrics = training_params.metrics
        self.input_shape = input_shape
        self.batch_size = training_params.batch_size
        self.epochs = training_params.epochs
        self.base_path = paths.dirs.root
        self.layers = layers
        self.paths = paths

    def build_from_config(self):
        """Build model from configuration file."""

        self.layers[0]['config']['input_shape'] = self.input_shape

        model = Sequential()
        for layer in self.layers:
            #  print("Adding {}".format(layer['layer']))
            #  print("Params: ", layer['config'])
            model.add(layers.deserialize({'class_name': layer['layer'],
                                          'config': layer['config']}))
        return model
