#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json
from keras.optimizers import Adadelta, Adam
from keras.losses import categorical_crossentropy, binary_crossentropy
from keras.utils import print_summary
from sklearn.metrics import (classification_report,
                             precision_recall_fscore_support,
                             confusion_matrix)

from keras.callbacks import ModelCheckpoint, CSVLogger
from beattracker.util.custom_callbacks import (SensitivitySpecificity,
                                               TimeHistory,
                                               ModelState,
                                               HistoryCheckPoint)
from beattracker.networks.network_helpers import (convert_to_single_column_binary)


class NN():
    """NN Class."""

    _optimizers = {"Adadelta": Adadelta(), "Adam": Adam()}
    _losses = {"categorical_crossentropy": categorical_crossentropy,
               "binary_crossentropy": binary_crossentropy}

    def return_optimizer(self, optimizer):
        return self.optimizers[optimizer]

    def build(self):
        pass

    def get_store_dir(self):
        """Return directory where architecture/version specifc files are stored."""
        return self.base_path

    def compile(self, model):
        """Compile model."""

        model.compile(loss=self.losses,
                      optimizer=self.optimizer,
                      metrics=[self.metrics])

        #  return model

    def save_model(self, model):
        model_json = model.to_json()

        with open(self.paths.files.model, 'w') as outfile:
            json.dump(model_json, outfile)

        print("Saved model to JSON")

    def train(self, model, training_generator, validation_generator):
        """Train model."""

        sensit_specit = SensitivitySpecificity(model, validation_generator)
        #  early_stopping = EarlyStopping(monitor='val_loss', min_delta=0.01,
        #                                 patience=8, verbose=1, mode='auto')

        # Checkpoint the model
        model_checkpoint = ModelCheckpoint(self.paths.files.model,
                                           monitor='val_loss',
                                           save_best_only=True,
                                           verbose=2,
                                           mode='min',
                                           save_weights_only=False)

        csv_logger = CSVLogger(self.paths.files.train_logs,
                               separator=',', append=True)

        model_state = ModelState(self.paths.files.state)
        # If we have trained previously, set up the model checkpoint so it won't save
        # until it finds something better. Otherwise, it would always save the
        # results of the first epoch.
        try:
            model_checkpoint.best = model_state.state['best_values']['val_loss']
        except KeyError:
            pass

        # Offset epoch counts if we are resuming training. If you don't do
        # this, only epochs-resume_epochs epochs will be done.
        resume_epoch = model_state.state['epoch_count']
        self.epochs -= resume_epoch

        hist = HistoryCheckPoint(self.paths.files.epoch_history, resume_epoch)
        time_callback = TimeHistory(self.paths.files.epoch_times, resume_epoch)

        callback_list = [model_checkpoint, model_state, csv_logger, sensit_specit,
                         hist, time_callback]
        print("Training...")
        model.fit_generator(generator=training_generator,
                            validation_data=validation_generator,
                            validation_steps=len(validation_generator) - 1,
                            use_multiprocessing=True,
                            workers=8,
                            steps_per_epoch=len(training_generator) - 1,
                            max_queue_size=32,
                            #  batch_size=self.batch_size,
                            epochs=self.epochs,
                            callbacks=callback_list,
                            verbose=1)

    def get_batch_y_true_y_pred(self, model, generator, num_batches):
        y_true = np.concatenate([generator[x][1] for x in range(num_batches)])
        y_pred = model.predict_generator(generator, num_batches)
        return y_true, y_pred

    def evaluate(self, model, test_generator, include_report=True, batches=500,
                 output_dict=False):
        """Evaluate model."""

        num_batches = np.minimum(len(test_generator), batches)
        print(f"Evaluating {num_batches} batches.")

        y_true, y_pred = self.get_batch_y_true_y_pred(
            model, test_generator, num_batches)

        y_true = convert_to_single_column_binary(y_true)
        y_pred = convert_to_single_column_binary(y_pred)

        score = model.evaluate_generator(test_generator)
        test_loss = score[0]
        test_accuracy = score[1]

        cm = confusion_matrix(y_true=y_true, y_pred=y_pred)

        if include_report:
            test_report = classification_report(y_true, y_pred,
                                                output_dict=output_dict)
            return {'loss': test_loss,
                    'acc': test_accuracy,
                    'cm': cm,
                    'test_rep': test_report}

        return {'loss': test_loss, 'acc': test_accuracy, 'cm': cm}

    def print_confusion_matrix(self, cm, normalize=False):
        """
            cm: confusion matrix
            cm[0][0] = tn
            cm[0][1] = fp
            cm[1][0] = fn
            cm[1][1] = tp
        """
        print('True Positive Rate',
              self.true_positive_rate(cm[1][1], cm[1][0]))
        print('True Negative Rate',
              self.true_negative_rate(cm[0][0], cm[0][1]))

        if normalize:
            print("Normalizing confusion matrix")
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print('Confusion matrix:\n', cm)

    def print_architecture(self, model):
        print_summary(model)

    def plot_history(self, history, filename=''):
        loss_list = [s for s in history['history'].keys()
                     if 'loss' in s and 'val' not in s]
        val_loss_list = [s for s in history['history'].keys()
                         if 'loss' in s and 'val' in s]
        acc_list = [s for s in history['history'].keys()
                    if 'acc' in s and 'val' not in s]
        val_acc_list = [s for s in history['history'].keys()
                        if 'acc' in s and 'val' in s]

        if len(loss_list) == 0:
            print('Loss is missing in history')
            return

        # As loss always exists
        epochs = range(1, len(history['history'][loss_list[0]]) + 1)

        # Loss
        plt.figure(1)
        plt.locator_params(axis='x', nbins=len(epochs))
        for l in loss_list:
            plt.plot(epochs, history['history'][l], 'b',
                     label='Training loss (' + str(str(
                         format(history['history'][l][-1], '.5f')) + ')'))

        for l in val_loss_list:
            plt.plot(epochs, history['history'][l], 'g',
                     label='Validation loss (' + str(str(format(
                         history['history'][l][-1], '.5f')) + ')'))

        plt.title('Loss')
        plt.suptitle(filename)
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.legend()

        # Accuracy
        plt.figure(2)
        plt.locator_params(axis='x', nbins=len(epochs))
        for l in acc_list:
            plt.plot(epochs, history['history'][l], 'b',
                     label='Training accuracy (' + str(format(
                         history['history'][l][-1], '.5f')) + ')')
        for l in val_acc_list:
            plt.plot(epochs, history['history'][l], 'g',
                     label='Validation accuracy (' + str(
                         format(history['history'][l][-1], '.5f')) + ')')

        plt.title('Accuracy')
        plt.suptitle(filename)
        plt.xlabel('Epochs')
        plt.ylabel('Accuracy')
        plt.legend()
        plt.show()

    def print_report(self, model, test_generator):

        y_true, y_pred = self.get_batch_y_true_y_pred(
            model, test_generator, 50)

        y_true = np.argmax(y_true, axis=-1)
        y_pred = np.argmax(y_pred, axis=-1)

        clf_rep = precision_recall_fscore_support(y_true, y_pred)

        out_dict = {"precision": clf_rep[0].round(2),
                    "recall": clf_rep[1].round(2),
                    "f1-score": clf_rep[2].round(2),
                    "support": clf_rep[3]}
        out_df = pd.DataFrame(out_dict)
        avg_tot = (out_df.apply(lambda x: round(x.mean(), 2)
                                if x.name != "support"
                                else round(x.sum(), 2)).to_frame().T)
        avg_tot.index = ["avg/total"]
        out_df = out_df.append(avg_tot)
        #  out_df.to_csv(self.paths.files.results)
        return out_df.to_string(), clf_rep

    def compile_logs(self):
        layer_summary = ''
        for layer in self.layers:
            layer_summary += layer['layer'] + ":\n"
            for k, v in layer['config'].items():
                layer_summary += "  " + k + ":" + str(v) + "\n"

        return {"version": self.version,
                "input_shape": self.input_shape,
                "nn_architecture": self.nn_architecture,
                "batch_size": self.batch_size,
                "epochs": self.epochs,
                "metrics": self.metrics,
                "optimizer": self.optimizer,
                "losses": self.losses,
                "layers": layer_summary
                }
