#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np


def true_positive_rate(tp, fn):
    return tp / (tp + fn)


def true_negative_rate(tn, fp):
    return tn / (tn + fp)


def false_negative_rate(fn, tp):
    return fn / (fn + tp)


def false_positive_rate(fp, tn):
    return fp / (fp + tn)


def calc_accuracy(tp, fn, fp, tn):
    return (tn + tp) / (tn + fp + fn + tp)


def calc_precision(tp, fp):
    return tp / (tp + fp)


def convert_to_single_column_binary(matrix):
    return np.argmax(matrix, axis=-1)
