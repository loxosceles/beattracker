#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path
from os import path

ROOT_DIR = path.dirname(path.dirname(path.abspath(__file__)))
STATIC_CFG = Path(Path.home() / ".config/beattracker/conf.json")
VERSION_FILE = Path(Path.home() / '.config/beattracker/version.json')
