#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Track the beat."""

import sys
from os import path, walk, environ
import matplotlib.pyplot as plt

from keras.models import load_model
from dot_configs.dot_configs import Configurations

from beattracker.constants import ROOT_DIR
from beattracker.tracker.track_object import TrackGenerator
from beattracker.postprocessing.prediction import slice_prediction, apply_threshold

# Disable the warning, doesn't enable AVX/FMA
environ["TF_CPP_MIN_LOG_LEVEL"] = "2"

STORE_PATH = '/mnt/DATA/IIMAS/THESIS_BEAT-TRACKING/Beattracking_Data/bt_store/cnn'


def list_models(store_path):
    models = {}
    count = 1
    for root, dirs, files in walk(store_path):
        dirs.sort()
        if 'model.hdf5' in files:
            try:
                models.setdefault(str(count), {
                    "config": path.join(root, "network_cfg.json")
                })
            except IndexError:
                continue

            models.get(str(count)).setdefault(
                "model", path.join(root, 'model.hdf5'))
            count += 1
    return models


def prompt_for_model(models):
    for idx, el in models.items():
        sys.stdout.write(f"{idx}: {el.get('model')}\n")
    model_index = input("Choose model by index: ")
    if model_index not in models.keys():
        raise ValueError("Model index wrong.")
    return model_index


def main(audio_path):
    audio_path = path.join(ROOT_DIR, audio_path)

    models = list_models(STORE_PATH)

    try:
        idx = prompt_for_model(models)
    except ValueError as e:
        print(e)
        sys.exit(1)

    model_path = models.get(idx).get('model')
    model_config_path = models.get(idx).get('config')

    try:
        model = load_model(model_path)
        cfg = Configurations(model_config_path).get_configuration()
        print(f"Loading {model_path}")
        print(f"Configurations: {model_config_path}")
    except ValueError as e:
        print(e)
        print("Model path not valid\n")
        sys.exit(1)

    input_shape = model.input_shape[1:]
    print(input_shape)

    audio_params = {"sample_rate": cfg.audio_params.sample_rate, "hop_size":
                    cfg.audio_params.hop_size,
                    "frame_size": cfg.audio_params.frame_size, "num_bands":
                    cfg.audio_params.num_bands}

    track_gen = TrackGenerator(audio_path, input_shape, **audio_params)

    pred = model.predict_generator(track_gen)
    pred = slice_prediction(pred)
    pred = apply_threshold(pred, 0.9)
    plt.plot(pred)
    plt.show()
    return track_gen, pred
