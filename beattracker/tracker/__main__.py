#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Tracker. Applies a model to a new audio file."""

import argparse
from inspect import cleandoc
from beattracker.tracker.track import main

help_audio_file = cleandoc(
    """
    Path to audio file.
    """
)

help_model_file = cleandoc(
    """
    Path to model.
    """
)

parser = argparse.ArgumentParser(
    description="Make prediction on audio file.")

parser.add_argument("audio_path", nargs="?", help=help_audio_file)
parser.add_argument("model_path", nargs="?", help=help_model_file)

args = vars(parser.parse_args())

main(args.get("audio_path"))
