#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import numpy as np

from keras.utils import Sequence
from madmom.audio.spectrogram import (LogarithmicFilteredSpectrogram,
                                      # Spectrogram, FilteredSpectrogram,
                                      SpectrogramDifference
                                      )
from madmom.audio.signal import Signal
from madmom.audio.filters import MelFilterbank


class TrackGenerator(Sequence):
    """Spectrograms, audio signal and properties for a specific file."""

    spectrogram_converters = {
        "log_filt": LogarithmicFilteredSpectrogram,
        "spec_diff": SpectrogramDifference
    }

    def __init__(self, audio_path, input_shape, **audio_params):
        self.audio_path = audio_path
        self.sample_rate = audio_params.get("sample_rate")
        self.num_bands = audio_params.get("num_bands")
        self.hop_size = audio_params.get("hop_size")
        self.frame_size = audio_params.get("frame_size")
        self.input_shape = input_shape
        self.chunk_size = input_shape[0]
        self.signal = self.convert_to_signal()

        if self.num_bands is None:
            self.spectrogram = TrackGenerator.spectrogram_converters.get(
                "log_filt")(self.signal,
                            frame_size=self.frame_size,
                            hop_size=self.hop_size)
        else:
            self.spectrogram = TrackGenerator.spectrogram_converters.get(
                "log_filt")(self.signal,
                            frame_size=self.frame_size,
                            hop_size=self.hop_size,
                            filterbank=MelFilterbank,
                            num_bands=self.num_bands, add=1)
        print(f"chunk size: {self.chunk_size}")
        print(f"Shape: {self.spectrogram.shape}")

    def __len__(self):
        return self.spectrogram.num_frames - self.chunk_size + 1

    def __getitem__(self, index):
        X = self.spectrogram[index:index +
                             self.chunk_size].reshape(self.input_shape)
        return np.expand_dims(X, axis=0)

    def convert_to_signal(self):
        """Load audio file and convert to madmom framed signal."""
        signal = Signal(self.audio_path,
                        sample_rate=self.sample_rate, num_channels=1)
        return signal
