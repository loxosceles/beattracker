#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Top-level package for BT_NN."""

__author__ = """Magnus Henkel"""
__email__ = 'loxosceles@gmx.de'
__version__ = '0.1.0'
