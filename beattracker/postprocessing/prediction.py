#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Post-processing module."""


def slice_prediction(matrix):
    return matrix[:, 1:].flatten()


def apply_threshold(matrix, threshold):
    matrix = matrix.copy()
    matrix[matrix >= threshold] = 1
    matrix[matrix < 1] = 0
    return matrix
