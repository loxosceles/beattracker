#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from keras.utils import Sequence
from beattracker.util.helpers import unpickle_file


class ChunkSizeParamWrong(Exception):
    def __init__(self, message):
        print(message)


class SampleSizeNotMatching(Exception):
    def __init__(self, message):
        print(message)


class GeneratorIndexOutOfRangeError(Exception):
    def __init__(self, message):
        print(message)


class DataGenerator(Sequence):
    """
    Data Generator.
    """

    reshape_function = {2: lambda x: x, 3: lambda x: np.expand_dims(x, axis=2)}
    activation_format = {2: {True: True, False: False},
                         3: {True: [0, 1], False: [1, 0]}}

    def __init__(self, gentype, mapping, frame_size, batch_size, output_activ_func,
                 training_samples, input_shape, shuffle=True):

        self.input_shape = input_shape
        self._reshape = DataGenerator.reshape_function[len(input_shape)]
        self.gentype = gentype
        self.list_ids = list(mapping.keys())
        self.mapping = mapping
        self.chunk_margin = training_samples.margin
        self.chunk_size = training_samples.chunk_size
        self.output_activf_idx = {
            "sigmoid": 2, "softmax": 3}[output_activ_func]
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.frame_size = frame_size
        self.on_epoch_end()

    def __len__(self):
        """Denote the number of batches per epoch."""
        return (len(self.list_ids) // self.batch_size) - 1

    def __getitem__(self, batch_index):
        """Generate a batch of data."""

        samples = np.empty(
            ((self.batch_size,) + self.input_shape))

        labels = np.empty((self.batch_size, 2))

        batch = self.get_list_ids_batch(batch_index)

        for idx, item in enumerate(batch):
            fobj = unpickle_file(self.mapping[item][0])
            frame_index = self.mapping[item][1]
            lower, upper = self.divide_chunk_size(self.chunk_size)
            full_spec = fobj.log_filt_specs[self.frame_size]
            chunk = self.create_spec_chunk(frame_index, full_spec, lower, upper)
            samples[idx] = self._reshape(chunk)
            hp_low, hp_high = self.determine_hotpatch_boundaries(frame_index,
                                                                 lower, upper,
                                                                 self.chunk_margin)
            onset = self.has_onset(fobj.onset_frames[self.frame_size],
                                   hp_low, hp_high)
            onset = DataGenerator.activation_format[self.output_activf_idx][onset]
            labels[idx] = onset

        return (samples, labels)

    def get_list_ids_batch(self, batch_index):
        """Return batch generator with a range of batch_size."""
        if self.__len__() < (batch_index):
            raise GeneratorIndexOutOfRangeError("Generator index out of range:\
                                                {}".format(batch_index))
        return self.list_ids[batch_index * self.batch_size:
                             (batch_index + 1) * self.batch_size]

    def divide_chunk_size(self, chunk_size):
        return (chunk_size // 2, chunk_size // 2)

    def determine_hotpatch_boundaries(self, frame_index, lower, upper, padding):
        l_bound = frame_index - lower + padding
        u_bound = frame_index + upper - padding
        return (l_bound, u_bound)

    def has_onset(self, onset_frames, hp_low, hp_high):
        return any(filter(lambda x: hp_low <= x < hp_high, onset_frames))

    def create_spec_chunk(self, frame_index, full_spec, lower, upper):
        return full_spec[frame_index - lower:frame_index + upper, :]

    def on_epoch_end(self):
        """Shuffle list_ids after each epoch."""
        if self.shuffle is True:
            np.random.shuffle(self.list_ids)
