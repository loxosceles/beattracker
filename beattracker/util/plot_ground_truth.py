#!/usr/bin/env python3
# Imports
import numpy as np
import sys
import matplotlib.pyplot as plt
sys.path.append('../madmom')
from file_object import FileObject

# Constants
audio_file = '../../annotated_audio/SMC_MIREX/SMC_MIREX_Audio/SMC_001.wav'
ANNOT_ROOT = '../../annotated_audio/SMC_MIREX/SMC_MIREX_Annotations/'

#def __init__(self, audio_file_path, annot_file_root):

def main():
    """docstring for main"""

    # get annotation file => list of beat at time position (min)
    frame_indices = []
    frame_rate = 44100 / 441 # 100
    with open(ANNOT_ROOT + 'SMC_001_2_1_1_a.txt', 'r') as infile:
       frame_indices = [ round(float(time) * frame_rate) for time in infile.readlines() ]

    print(frame_indices)

    # create numpy zero array and fill in the frame indices
    time_line = np.zeros([1, 4000])
    for el in frame_indices:
        time_line[0][el] = 1.0

    # plot the graph
    #  plt.plot(time_line[0])
    #  plt.show()
    return time_line[0]


if __name__ == '__main__':
    main()
