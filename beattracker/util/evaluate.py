#!/usr/bin/env python3

from os import path
import logging
import argparse
from inspect import cleandoc
from beattracker.beattracker import define_input_shape
from dot_configs.dot_configs import Configurations
from beattracker.util.data_generator import DataGenerator
from beattracker.networks.nn import NN
from keras.models import load_model
from beattracker.util.helpers import (unpickle_file,
                                      pickle_file,
                                      filter_by_key,
                                      filter_invalid_indices,
                                      format_eval_output)


def write_test_report(log_events, logs_path):

    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    logging.basicConfig(format='%(message)s', filename=logs_path,
                        filemode='w',
                        level=logging.INFO)

    log_body = cleandoc("""
{separator}
Confusion Matrix:
-----------------
{cm}

Report:
-------
Test Loss:          {loss}
Test Accuracy:      {acc}

{test_rep}
{separator}
""".format(separator=65 * '*', **log_events))

    logging.info(log_body)


def main(configs):
    print(configs)

    index = configs.paths.run.pickle_index.replace('/training/', '/testing/')
    print("Loading: ", index)

    indices = unpickle_file(index)

    arch = configs.args.deep_get(['architecture'])
    frame_size = configs.architecture.deep_get([arch]).audio_params.frame_size
    batch_size = configs.architecture.deep_get(
        [arch]).training_params.batch_size
    input_shape = define_input_shape(indices,
                                     frame_size,
                                     configs)
    print("Defined input shape to: ", input_shape)
    nn_arch = configs.deep_get(['architecture', configs.args.architecture])
    nn_arch.set('input_shape', input_shape)

    indices = filter_invalid_indices(indices, configs.training_data.chunk_size)

    test_indices = list(indices.keys())
    test_mapping = filter_by_key(test_indices, indices)

    testgen = DataGenerator(test_indices, test_mapping, frame_size,
                            batch_size, "softmax", configs.training_data,
                            nn_arch)

    model_path = path.join(configs.paths.run.run_dir,
                           configs.paths.files.model)
    print("Loading model: ", model_path)
    model = load_model(model_path)

    evaluation_data = NN.evaluate(model, testgen, True, len(testgen) - 2, True)
    print()
    print("Test loss: ", evaluation_data['loss'])
    print("Test accuracy: ", evaluation_data['acc'])
    print(format_eval_output(evaluation_data['test_rep']))
    NN.print_confusion_matrix(evaluation_data['cm'], normalize=True)

    evaluation_path = path.join(configs.paths.run.run_dir,
                                configs.paths.files.test_eval_report)

    pickle_file(evaluation_data, evaluation_path)
    #  write_test_report(evaluation_data, report_path)
    return model, testgen


if __name__ == '__main__':
    # Arguments
    help_c = cleandoc("""
                Select configuration file.
             """)

    parser = argparse.ArgumentParser(
        description="Evaluate model with test dataset.")
    parser.add_argument('-c', '--Configuration-file',
                        required=True, help=help_c)

    args = vars(parser.parse_args())

    #  add_args_to_configs(args, configs)

    config_path = args['configuration-file']
    configs = Configurations(config_path).get_configuration()

    main(configs)
