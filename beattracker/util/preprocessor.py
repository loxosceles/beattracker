#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path
from os import path
import argparse
from beattracker.util.conf_helpers import build_path_object
from beattracker.util.file_object import FileObject
from dot_configs.dot_configs import Configurations
from beattracker.util.helpers import (pickle_file, silent_create, create_file_list,
                                      add_args_to_configs)
from inspect import cleandoc
from tqdm import tqdm


def update_mapping(mapping, num_frames, pickle_path):
    """
    Update mapping file with new sequence of frame indices belonging to the new file.

    PARAMETERS
        :mapping: mapping dictionary
        :num_frames: number of frames by which the signal of the file obeject is
        divided by
        :pickle_path: path of the pickle file of the file object

    """
    count = len(mapping)
    mapping.update({
        num: (pickle_path, num - count)
        for num
        in range(count, count + num_frames)
    })


def compose_annotation_path(file_pattern, annotation_dir):
    """Compose annotation file path for audio file."""
    return path.join(annotation_dir, file_pattern + '.txt')


def extract_file_pattern(file_path):
    """Extract file pattern (file name without extension)."""
    return ".".join(file_path.split('/')[-1].split('.')[:-1])


def pickle_dataset(file_list, paths, audio_params):
    """Pickle every file of file list.

    PARAMETERS
        :file_list: list of files in the audio directory
        :paths: paths object
        :audio_params: audio parameters subtree of configs

    RETURNS
        dictionary of mappings

    """
    mapping_dict = {}
    for file_path in tqdm(file_list):
        file_pattern = extract_file_pattern(file_path)
        annot_path = compose_annotation_path(
            file_pattern, paths.dirs.annotations)

        fobj = FileObject(file_path, annot_path,
                          audio_params.hop_size,
                          audio_params.num_bands,
                          audio_params.frame_sizes)

        # FIXME; Should be part of the paths object
        pickle_path = Path(paths.dirs.pickle, file_pattern + '.pickle')

        # Pickle the data
        pickle_file(fobj, pickle_path)

        # num_frames depends on hop size, so any frame size serves
        num_frames = fobj.log_filt_specs[1024].num_frames
        update_mapping(mapping_dict, num_frames, pickle_path)

    return mapping_dict


def calculate_limit(number_of_files, percentage):
    """Calculate number of files for limited dataset.

    PARAMETERS:
        :number_of_files: length of file list (int)
        :percentaje: percentage to which the limited dataset will be reduced to
        (float)

    RETURNS:
        number of files to be processed (int)

    """
    if percentage < 0 or percentage >= 1:
        raise ValueError("Percentage value must be between 0 and 1")

    return int(number_of_files * percentage)


def generate_limited_mappings(mapping, limit):
    """Cut out mappings for a limited set of files.

    PARAMETERS
        :mapping: dictionary of all mappings
        :limit: number of files that form the limited dataset
    RETURNS
        dictionary of limited mappings

    """
    count = 1
    file_count = 0
    mapping_size = len(mapping)
    for i in range(mapping_size - 1):
        if list(mapping.values())[i][1] != list(mapping.values())[i + 1][1] - 1:
            file_count += 1
            if file_count == limit:
                break
        count += 1

    return {k: mapping.get(k) for k in range(count)}


def main(paths, configs):

    file_list = create_file_list(paths.dirs.audio)

    print(f"Procesing {len(file_list)} files.")
    print(f"Saving pickle files to {paths.dirs.pickle}")

    silent_create(paths.dirs.pickle)

    mapping = pickle_dataset(file_list, paths, configs.audio_params)
    print("Length of mapping: ", len(mapping))
    pickle_file(mapping, paths.dirs.pickle, 'index.pickle')

    limit = calculate_limit(len(file_list), 0.2)
    print(f"Generating limited mapping for {limit} files.")
    mapping_limited = generate_limited_mappings(mapping, limit)
    pickle_file(mapping_limited, paths.dirs.pickle, 'index_limited.pickle')


if __name__ == '__main__':

    help_c = cleandoc("""
                Configuration file.
                JSON file in which audio and training paramters for the preprocessing
                set are defined.
             """)

    parser = argparse.ArgumentParser(description="Pickling data files.")

    parser.add_argument('configuration-files', help=help_c)

    args = vars(parser.parse_args())

    configs = Configurations(args['configuration-files']).get_configuration()

    paths = build_path_object(configs, configs.args)

    add_args_to_configs(args, configs)

    main(paths, configs)
