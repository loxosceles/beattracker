#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from madmom.audio.spectrogram import (LogarithmicFilteredSpectrogram,
                                      # Spectrogram, FilteredSpectrogram,
                                      #  SpectrogramDifference
                                      )
from madmom.audio.signal import Signal  # FramedSignal
from madmom.audio.filters import MelFilterbank

from decimal import Decimal, getcontext
import matplotlib.pyplot as plt
import numpy as np

getcontext().prec = 4


class FileObject:
    """Information relevant to a specific file."""

    def __init__(self, audio_path, annot_path, hop_size, num_bands, frame_sizes,
                 sample_rate=22050):
        self.audio_path = audio_path
        self.annot_path = annot_path
        self.onset_times = []
        self.hop_size = hop_size
        self.num_bands = num_bands
        self.sample_rate = sample_rate
        self.signal = Signal(audio_path, sample_rate=sample_rate)
        self.num_samples = self.signal.num_samples

        self.log_filt_specs = {}

        for fs in frame_sizes:
            if self.num_bands == 0:
                self.log_filt_specs[fs] =\
                    LogarithmicFilteredSpectrogram(self.signal,
                                                   frame_size=fs,
                                                   hop_size=self.hop_size)
                #  SpectrogramDifference(LogarithmicFilteredSpectrogram(self.audio_path,
            else:
                self.log_filt_specs[fs] =\
                    LogarithmicFilteredSpectrogram(self.signal,
                                                   frame_size=fs,
                                                   hop_size=self.hop_size,
                                                   filterbank=MelFilterbank,
                                                   num_bands=self.num_bands, add=1)
                #  SpectrogramDifference(LogarithmicFilteredSpectrogram(self.audio_path,

        self.parse_onsets_from_file()
        self.onset_frames = {k: [] for k in frame_sizes}
        self.attach_onsets()

    def attach_onsets(self):
        for key in self.log_filt_specs:

            for onset_t in self.onset_times:
                #  print("Onset time: ", onset_t)

                s = self.time_to_sample(onset_t)
                #  print("Onset is located on {} sample.".format(s))

                f_index = self.sample_to_frame_index(s)
                #  print("Frame index of last frame in which onset occurs:
                #  {}".format(f_index))

                f0_idx, f0_overlap = self.divide_float(f_index)
                #  print("F0 index: {}, f0 overlap: {}".format(f0_idx, f0_overlap))

                onset_frames = self.get_onset_frames(f0_idx, f0_overlap, key)
                #  print("List of frames with onset: ", onset_frames)

                self.onset_frames[key] += onset_frames
                #  print("frd_sig", frd_sig.onset_frames)

    def time_to_sample(self, t):
        return np.round(self.sample_rate * t).astype(int)

    def sample_to_frame_index(self, sample):
        return sample / self.hop_size

    def calc_overlap(self, predecessor_index, frame_size):
        return frame_size - (predecessor_index * self.hop_size)

    def divide_float(self, f):
        f = Decimal(f)
        decimal = f - int(f)
        integer = int(f - decimal)
        return integer, float(decimal)

    def get_onset_frames(self, f0_idx, f0_olap, frame_size):
        frame_list = [f0_idx]
        f0_olap_samples = self.hop_size * f0_olap
        #  print("F0 overlap samples: ", f0_olap_samples)

        for i in range(1, f0_idx):
            fx_olap = self.calc_overlap(i, frame_size)
            if fx_olap >= f0_olap_samples:
                frame_list.insert(0, f0_idx - i)
            else:
                break

        return frame_list

    def parse_onsets_from_file(self):
        with open(self.annot_path, "r") as infile:
            self.onset_times = list(float(line) for line in infile.readlines())

    @classmethod
    def plot(cls, spec):
        plt.matshow(spec.T, origin='lower', aspect='auto', interpolation=None)
        plt.show()
