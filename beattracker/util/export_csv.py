#!/usr/bin/env python3

from os import path
import csv
import argparse
from inspect import cleandoc
from dot_configs.dot_configs import Configurations
from beattracker.util.helpers import unpickle_file


def main(configs):
    arch = configs.architecture.deep_get([configs.args.architecture])
    file_name = configs.paths.run.run_config.split('/')[-1]
    print("Processing: {}".format(file_name))

    evaluation_path = path.join(configs.paths.run.run_dir,
                                configs.paths.files.test_eval_report)

    try:
        test_results = unpickle_file(evaluation_path)
    except FileNotFoundError:
        print("Please run evaluations first!")
        import sys
        sys.exit()

    print(test_results)

    f = csv.writer(open(path.join(configs.paths.run.run_dir, 'main_params.csv'), "w",
                        newline=''))

    file_path = path.join(configs.paths.run.run_dir, file_name)

    header_list = [
        "file path",
        "file_name",
        "epochs",
        "limit files",
        "batch_size",
        "optimizer",
        "frame_size",
        "spec_type",
        "hop_size",
        "num_bands",
        "chunk_size",
        "margin",
        "0 prec",
        "1 prec",
        "avg prec",
        "0 recall",
        "1 recall",
        "avg recall",
        "0 f1-score",
        "1 f1-score",
        "avg f1-score",
        "0 support",
        "1 support",
        "% 0",
        "% 1",
        "avg support",
        "test_accuracy",
        "test_loss",
        "TN",
        "FP",
        "FN",
        "TP"
    ]

    value_list = [
        file_path,
        file_name,
        arch.training_params.epochs,
        configs.args.limit_files,
        arch.training_params.batch_size,
        arch.training_params.optimizer,
        arch.audio_params.frame_size,
        arch.audio_params.spec_type,
        configs.spectrograms.hop_size,
        configs.spectrograms.num_bands,
        configs.training_data.chunk_size,
        configs.training_data.padding,
        test_results['test_rep']['0']['precision'],
        test_results['test_rep']['1']['precision'],
        test_results['test_rep']['weighted avg']['precision'],
        test_results['test_rep']['0']['recall'],
        test_results['test_rep']['1']['recall'],
        test_results['test_rep']['weighted avg']['recall'],
        test_results['test_rep']['0']['f1-score'],
        test_results['test_rep']['1']['f1-score'],
        test_results['test_rep']['weighted avg']['f1-score'],
        test_results['test_rep']['0']['support'],
        test_results['test_rep']['1']['support'],
        "",
        "",
        test_results['test_rep']['weighted avg']['support'],
        test_results['acc'],
        test_results['loss'],
        test_results['cm'][0][0],
        test_results['cm'][0][1],
        test_results['cm'][1][0],
        test_results['cm'][1][1]
    ]

    f.writerow(header_list)
    f.writerow(value_list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Extract configurations to csv.")
    help_f = cleandoc("""
                File name.
             """)
    parser.add_argument('-f', '--file', required=True, help=help_f)
    args = vars(parser.parse_args())
    config_file = args["file"]

    configs = Configurations(config_file).get_configuration()

    main(configs)
