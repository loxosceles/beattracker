#!/usr/bin/env python3

from pathlib import Path
import json
from os import path
from dot_configs.dot_configs import Configurations

from beattracker.constants import STATIC_CFG, VERSION_FILE


def get_seq(vers_num, versions):
    if not isinstance(vers_num, str):
        raise TypeError("Version number must be of type <str>")

    value = versions.get(vers_num, 0)
    value += 1
    versions[vers_num] = value

    return str(value)


def zero_pad(number, digits=3):
    # FIXME: Can be done like this:  %03d' % 32
    number = str(number)
    return ((digits - len(number)) * "0") + number


def build_version_sequence_sting(version, sequence, version_dict):
    if sequence:
        seq = zero_pad(sequence)
    else:
        sequence = get_seq(version, version_dict)
        seq = zero_pad(sequence)

    vers = zero_pad(version)

    return "v" + vers + "s" + seq


def build_run_dir_path(root, vers_seq_string):
    run_dir = Path(root, vers_seq_string)
    return run_dir


def build_run_file_path(root, vers_seq_string, architecture):
    run_dir = Path(root, vers_seq_string)
    return Path(run_dir, vers_seq_string + "_" + architecture.upper() + ".json")


def build_root_dir_path(args_switch, dirs_base_path, args, architecture):
    store = args_switch["sandbox"][args.sandbox]
    return Path(dirs_base_path, store, architecture)


def build_pickle_dir_path(pickle_root, dataset, hop_size, num_bands):
    hop_size = hop_size
    num_bands = num_bands
    props_ext = Path(dataset, "hop_size_{}".format(str(hop_size)),
                     "num_bands_{}".format(str(num_bands)))
    pickle = Path(pickle_root / props_ext)
    return pickle


def build_pickle_index_path(args_switch, args, pickle_path):
    pickle_index = args_switch["limit_files"][args.limit_files]
    pickle_index = Path(pickle_path, pickle_index)
    return pickle_index


def insert_path_variable(path, placeholder, replacement):
    return path.replace(placeholder, replacement)


def build_generic_path(base_path, audio_dir):
    audio_path = Path(base_path, audio_dir)
    return audio_path


def build_path_object(cfg, args, st_cfg=None, versions=None):
    """
    Build path object from network configuration and static configurations.

    PARAMETERS
        :cfg: network configuration
        :args: command line arguments (may be stored in configuration file)
        :static_cfg: configs object with static paths, set when users runs the
            install script :version_file: dict of versions in the format: {"version":
            "count" }

    RETURNS
        :paths: config object with generated paths for a specific run

    """
    if st_cfg is None:
        st_cfg = Configurations(STATIC_CFG).get_configuration()

    version_file = path.join(st_cfg.dirs.configs, 'version')

    if versions is None:
        try:
            with open(version_file, 'r') as infile:
                versions = json.load(infile)
        except FileNotFoundError:
            versions = {}

    p_dict = {"files": {}, "dirs": {}}
    paths = Configurations(p_dict).get_configuration()

    args_switch = {"sandbox": {True: st_cfg.dirs.sandbox,
                               False: st_cfg.dirs.store},
                   "limit_files": {True: st_cfg.files.pickle_index_limited,
                                   False: st_cfg.files.pickle_index}}

    if Path(version_file).exists():
        d = {}
        with open(version_file, 'w') as outfile:
            json.dump(d, outfile)

    #  Build dir and run_file identifier
    vers_seq_string = build_version_sequence_sting(cfg.meta.version,
                                                   cfg.meta.sequence,
                                                   versions)

    #  Set pickle dir
    pickle_dir = build_pickle_dir_path(st_cfg.dirs.pickle,
                                       args.dataset,
                                       cfg.audio_params.hop_size,
                                       cfg.audio_params.num_bands)
    paths.dirs.set('pickle', str(pickle_dir))

    #  Set root dir
    root = build_root_dir_path(args_switch,
                               st_cfg.dirs.data,
                               args,
                               cfg.meta.architecture)
    paths.dirs.set('root', str(root))

    #  Set run dir
    run_dir = build_run_dir_path(paths.dirs.root, vers_seq_string)
    paths.dirs.set('run_dir', str(run_dir))

    #  Set pickle index
    pickle_index = build_pickle_index_path(args_switch,
                                           args,
                                           paths.dirs.pickle)
    paths.files.set('pickle_index', str(pickle_index))

    #  Set run_file
    run_file = build_run_file_path(paths.dirs.root,
                                   vers_seq_string,
                                   cfg.meta.architecture)
    paths.files.set('run_file', str(run_file))

    #  Set mappings file
    mappings_file = build_generic_path(paths.dirs.run_dir,
                                       st_cfg.files.mappings)
    paths.files.set('mappings', str(mappings_file))

    #  Set audio path
    audio = insert_path_variable(st_cfg.dirs.audio,
                                 "{{DATASET}}",
                                 args.dataset)
    audio_path = build_generic_path(st_cfg.dirs.data, audio)
    paths.dirs.set("audio", str(audio_path))

    #  Set annotation path
    annotations = insert_path_variable(st_cfg.dirs.annotations,
                                       "{{DATASET}}",
                                       args.dataset)
    annotation_path = build_generic_path(st_cfg.dirs.data,
                                         annotations)
    paths.dirs.set("annotations", str(annotation_path))

    #  Set queue dir (run configs)
    queue = build_generic_path(st_cfg.dirs.data,
                               st_cfg.dirs.queue)
    paths.dirs.set("queue", str(queue))

    #  Set predictions dir
    predictions = build_generic_path(st_cfg.dirs.data,
                                     st_cfg.dirs.predictions)
    paths.dirs.set("predictions", str(predictions))

    #  Set queue logs
    logs = Path(paths.dirs.queue, "logs")
    paths.dirs.set("queue_logs", str(logs))

    #  #  Set test_env path
    #  paths.dirs.set("test_env", st_cfg.dirs.test_env)

    #  Set all result files
    for key, value in st_cfg.files.results.items():
        paths.files.set(key, str(Path(paths.dirs.run_dir, value)))

    # Save version file
    with open(version_file, 'w') as outfile:
        json.dump(versions, outfile)

    return paths
