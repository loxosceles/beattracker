#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import path, makedirs, remove, walk, listdir
from shutil import copy
import contextlib
import pickle


def pickle_file(obj, pickle_dir, file=None, verbose=False):
    if file is None:
        full_path = pickle_dir
    else:
        full_path = path.join(pickle_dir, file)

    with open(full_path, 'wb') as handle:
        pickle.dump(obj, handle, protocol=pickle.HIGHEST_PROTOCOL)

    if verbose is True:
        if path.exists(full_path):
            print("Sucessfully pickled")


def unpickle_file(pickle_dir, file=None):
    if file is None:
        full_path = pickle_dir
    else:
        full_path = path.join(pickle_dir, file)

    try:
        with open(full_path, 'rb') as handle:
            m = pickle.load(handle)
        return m
    except FileNotFoundError:
        import sys
        print("File in {} do not exist!".format(pickle_dir))
        sys.exit()


def silent_create(directory):
    with contextlib.suppress(FileExistsError):
        makedirs(directory)
    return directory


def silent_copy(filename, dest):
    with contextlib.suppress(FileNotFoundError):
        copy(filename, dest)


def silent_remove(filename):
    with contextlib.suppress(FileNotFoundError):
        remove(filename)


def remove_files(directory):
    files_to_delete = ('.json', '.csv', '.hdf5', '.log', '.pk')

    for root, dirs, files in walk(directory):
        for file in files:
            if file.endswith(files_to_delete):
                remove(path.join(root, file))


def remove_confirm(directory, confirmation=True):
    """Remove directory asking for permission previously."""
    if confirmation is True:
        confirm = input("Removing contents of {}? (y/n): ".format(directory))

        while confirm != "y" or confirm != "n":
            if confirm == "y":
                remove_files(directory)
                break
            elif confirm == "n":
                print("Nothing removed. Will load objects and results if found.")
                break
            else:
                confirm = input(
                    "Removing contents of {}? (y/n): ".format(directory))
                continue
    else:
        remove_files(directory)
        print("All data removed!")


def create_file_list(audio_path):
    return [path.join(audio_path, f) for f in listdir(audio_path) if
            path.isfile(path.join(audio_path, f))]


def add_args_to_configs(args, configs):
    configs.set('args')
    for k, v in args.items():
        configs.args.set(k, v)


def _count_groups(onsets):
    d = {}
    group_no = 1
    for i in range(1, len(onsets)):
        if onsets[i] - onsets[i - 1] == 1:
            group_no += 1
            if i == len(onsets) - 1:
                d[group_no] = d.get(group_no, 0) + 1
        else:
            d[group_no] = d.get(group_no, 0) + 1
            group_no = 1
    return d


def create_group_dict(onset_frames):
    group_dict = {}
    for el in [1024, 2048, 4096]:
        group_dict[el] = _count_groups(onset_frames[el])
    return group_dict


def define_valid_frame_range(fobj, margin, frame_size):
    valid_frames = (
        margin, fobj.log_filt_specs[frame_size].num_frames - margin)
    return valid_frames


def define_valid_onset_frames(fobj, valid_frame_range):
    d = {}
    for frame_size in fobj.onset_frames.keys():
        #  print("frame_size: ", fobj.onset_frames[frame_size])
        d[frame_size] = [frame for frame in fobj.onset_frames[frame_size]
                         if frame >= valid_frame_range[0] and
                         frame <= valid_frame_range[1]]
    return d


def count_positive_frames(groups):
    count = 0
    d = {}
    for el in groups.keys():
        for k, v in groups[el].items():
            count += (v * (k - 1)) + v
        d[el] = count
        count = 0
    return d


def format_eval_output(d):
    dd = {}
    for key, value in d.items():
        for kkey, vvalue in value.items():
            dd[key + '-' + str(kkey)] = value[kkey]

    return """
       Test report

                     precision      recall     f1-score     support
              0           {0-precision:.2f}        {0-recall:.2f}         {0-f1-score:.2f}       {0-support}
              1           {1-precision:.2f}        {1-recall:.2f}         {1-f1-score:.2f}       {1-support}

      micro avg           {micro avg-precision:.2f}        {micro avg-recall:.2f}         {micro avg-f1-score:.2f}       {micro avg-support}
      macro avg           {macro avg-precision:.2f}        {macro avg-recall:.2f}         {macro avg-f1-score:.2f}       {macro avg-support}
   weighted avg           {weighted avg-precision:.2f}        {weighted avg-recall:.2f}         {weighted avg-f1-score:.2f}       {weighted avg-support}
    """.format(**dd)
