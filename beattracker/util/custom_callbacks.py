#!/usr/bin/env python3

# Imports
from os import path
import pickle
import numpy as np
import time
import json
from keras.callbacks import Callback
from sklearn.metrics import confusion_matrix

def pickle_file(obj, pickle_dir, file=None):
    if file is None:
        full_path = pickle_dir
    else:
        full_path = path.join(pickle_dir, file)

    with open(full_path, 'wb') as handle:
        pickle.dump(obj, handle, protocol=pickle.HIGHEST_PROTOCOL)

def unpickle_file(pickle_dir, file=None):
    if file is None:
        full_path = pickle_dir
    else:
        full_path = path.join(pickle_dir, file)

    try:
        with open(full_path, 'rb') as handle:
            m = pickle.load(handle)
        return m
    except FileNotFoundError as e:
        raise FileNotFoundError()

def get_batch_y_true_y_pred(model, generator, num_batches):
    num_batches = np.minimum(len(generator), num_batches)
    y_true = np.concatenate([generator[x][1] for x in range(num_batches)])
    y_pred = model.predict_generator(generator, num_batches)
    return y_true, y_pred

def print_confusion_matrix(cm, normalize=False):
    """
        cm: confusion matrix
        cm[0][0] = tn
        cm[0][1] = fp
        cm[1][0] = fn
        cm[1][1] = tp
    """
    print('Sensitivity', cm[1, 1] / (cm[1, 1] + cm[1, 0]))
    print('Specificity', cm[0, 0] / (cm[0, 0] + cm[0, 1]))

    if normalize:
        print("Normalizing confusion matrix")
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print('Confusion matrix:\n', cm)


class ModelState(Callback):
    def __init__(self, state_path):
        self.state_path = state_path

        self.load_or_initialize()

    def load_or_initialize(self):
        try:
            with open(self.state_path, 'r') as handle:
                self.state = json.load(handle)
            print("Loading state")
        except FileNotFoundError as e:
            self.state = {'epoch_count': 0,
                          'best_values': {},
                          'best_epoch': {}
                          }
            print("Initializing state")

    #  def on_train_begin(self, logs={}):
    #      pass

    def on_epoch_end(self, batch, logs={}):
        # Currently, for everything we track, lower is better
        for k in logs:
            if k not in self.state['best_values'] or\
                    logs[k] < self.state['best_values'][k]:

                self.state['best_values'][k] = float(logs[k])
                self.state['best_epoch'][k] = self.state['epoch_count']

        self.state['epoch_count'] += 1
        print('Completed epoch', self.state['epoch_count'])

        with open(self.state_path, 'w') as f:
            json.dump(self.state, f, indent=4)


class SensitivitySpecificity(Callback):
    def __init__(self, model, generator):
        self.model = model
        self.generator = generator

    def provide_batch(self, num_batches):
        x_test = np.concatenate([self.generator[x][0] for x in range(num_batches)])
        y_test = np.concatenate([self.generator[x][1] for x in range(num_batches)])
        return x_test, y_test

    def on_epoch_end(self, epoch, logs=None):
        if epoch % 4 == 0:
            y_true, y_pred = get_batch_y_true_y_pred(self.model,
                                                     self.generator,
                                                     50)
            y_true = np.argmax(y_true, axis=-1)
            y_pred = np.argmax(y_pred, axis=-1)
            cm = confusion_matrix(y_true=y_true, y_pred=y_pred)

            print_confusion_matrix(cm, False)


class HistoryCheckPoint(Callback):
    def __init__(self, history_path, resume_epoch):
        self.history_path = history_path
        self.resume_epoch = resume_epoch
        self.categories = ['acc', 'loss', 'val_acc', 'val_loss']
        try:
            with open(history_path, 'rb') as handle:
                self.history = pickle.load(handle)
        except FileNotFoundError as e:
            self.history = {
                "acc": {},
                "loss": {},
                "val_acc": {},
                "val_loss": {}
            }

    #  def on_epoch_begin(self, epoch, logs={}):
    #      # Things done on beginning of epoch.
    #      return

    def create_hist(self):
        hist = {}
        for el in self.categories:
            hist[el] = []
            for i in range(len(self.history[el].keys())):
                hist[el].append(self.history[el][i])
        return hist

    def on_epoch_end(self, epoch, logs={}):
        for el in self.categories:
            self.history[el][self.resume_epoch + epoch] = logs.get(el)
        self.history['history'] = self.create_hist()
        pickle_file(self.history, self.history_path)


class TimeHistory(Callback):
    def __init__(self, epoch_times_path, resume_epoch):
        try:
            self.times = unpickle_file(epoch_times_path)
            #  with open(epoch_times_path, 'rb') as handle:
            #      self.times = pickle.load(handle)
        except FileNotFoundError as e:
            self.times = {}

        self.epoch_times_path = epoch_times_path
        self.resume_epoch = resume_epoch

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.times[self.resume_epoch + epoch] = time.time() - self.epoch_time_start
        pickle_file(self.times, self.epoch_times_path)
