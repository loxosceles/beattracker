#!/usr/bin/env python3

# Imports
import json
from os import path
from beattracker.util.helpers import create_file_list

def extract_file(f, only_beat=True):
    beat_types = {}
    for el in f['annotations']:
        annot = el.get('sandbox').get('annotation_type')
        data = el.get('data')
        if annot is not None and data is not None:
            beat_types[annot] = list(map(lambda x: dict(x)['time'],
                                         data))

    file_name = f['file_metadata']['identifiers']['filename']
    file_name = ".".join(file_name.split('.')[:-1]) + '.au'
    if only_beat is True:
        return beat_types['beat'], file_name

def main():
    jams_dir = '/mnt/DATA/IIMAS/Beattracking_Data/annotated_audio/GTZAN/GTZAN-Rhythm_v2_ismir2015_lbd/jams/'
    readable_dir = '/mnt/DATA/IIMAS/Beattracking_Data/READABLE/GTZAN/Annotations'

    jam_files = create_file_list(jams_dir)

    for file in jam_files:
        with open(file, 'r') as infile:
            f = json.load(infile)
            beat_types, file_name = extract_file(f)
            print(file_name)
            file_name = file_name[:-3] + '.txt'
            print(file_name)
        with open(path.join(readable_dir, file_name), 'w+') as outfile:
            for el in beat_types:
                outfile.write(str(el) + '\n')

    return beat_types, file_name
