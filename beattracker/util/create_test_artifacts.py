#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from keras.models import load_model
from os import path
import json
import dill
import numpy as np
from dot_configs.dot_configs import Configurations

from beattracker.constants import ROOT_DIR
from beattracker.trainer.train import load_module
from beattracker.util.helpers import unpickle_file, pickle_file, silent_create
#  from beattracker.util.preprocessor import main as preprocess_audio
from beattracker.util.conf_helpers import build_path_object
from beattracker.util.data_generator import DataGenerator

from beattracker.trainer.train import (determine_input_shape,
                                       filter_invalid_indices,
                                       split_dataset)


TEST_DIR = path.join(ROOT_DIR, 'test')
TEST_DATA = path.join(TEST_DIR, 'data')


def fill_in_placeholders(_dict, placeholder, replacement):
    return json.loads(json.dumps(_dict).replace("{{" + placeholder + "}}", replacement))


def build_run_env(paths):
    for p in paths.dirs.values():
        try:
            silent_create(p)
        except FileNotFoundError:
            pass


#########################################################
# Build network, static configurations and path objects #
#########################################################

network_cfg_path = path.join(TEST_DATA, 'json/cnn.json')
network_cfg = Configurations(network_cfg_path).get_configuration()

static_cfg_path = path.join(TEST_DATA, 'conf/conf.json')

with open(static_cfg_path, "r") as infile:
    static_cfg_dict = json.load(infile)

static_cfg_dict = fill_in_placeholders(
    static_cfg_dict, "TEST_DATA_DIR", TEST_DATA)

static_cfg = Configurations(static_cfg_dict).get_configuration()

version = {}
paths = build_path_object(network_cfg, network_cfg.args, st_cfg=static_cfg,
                          versions=version)

#########################
#  Create pickle files  #
#########################

# preprocess_audio(paths, network_cfg)

############################
#  Create data generators  #
############################
print("Creating data generators")

mapping = unpickle_file(path.join(paths.files.pickle_index))

frame_size = network_cfg.audio_params.frame_size
batch_size = network_cfg.training_params.batch_size

input_shape = determine_input_shape(mapping,
                                    frame_size,
                                    network_cfg.meta.architecture,
                                    network_cfg.training_samples.chunk_size)

network_cfg.training_samples.set('input_shape', input_shape)

mapping = filter_invalid_indices(
    mapping, network_cfg.training_samples.chunk_size)

mapping = split_dataset(mapping, network_cfg.data_partitions.val_size,
                        network_cfg.data_partitions.test_size,
                        shuffle=False, random_state=42)


data_generators = {}
data_generators.setdefault('training',
                           DataGenerator("training",
                                         mapping["train_index"],
                                         frame_size,
                                         batch_size,
                                         "softmax",
                                         network_cfg.training_samples,
                                         input_shape,
                                         shuffle=False))
data_generators.setdefault('validation',
                           DataGenerator("validation",
                                         mapping["val_index"],
                                         frame_size,
                                         batch_size,
                                         "softmax",
                                         network_cfg.training_samples,
                                         input_shape,
                                         shuffle=False))
data_generators.setdefault('testing',
                           DataGenerator("testing",
                                         mapping["test_index"],
                                         frame_size,
                                         batch_size,
                                         "softmax",
                                         network_cfg.
                                         training_samples,
                                         input_shape,
                                         shuffle=False))

# Save to disk
data_generator_path = path.join(TEST_DATA, 'data_generators')
silent_create(data_generator_path)

for dgen in data_generators.values():
    with open(path.join(data_generator_path, dgen.gentype + '_gen'), 'wb+') as outfile:
        dill.dump(dgen, outfile)


##################
#  Get batch  #
##################
print("Getting sample batch")
# Get training generator, 8th batch
batch = data_generators.get('training')[8]

# Save to disk
batch_path = path.join(TEST_DATA, 'batch')
silent_create(batch_path)

pickle_file(batch, path.join(
    batch_path, 'train_gen_32_8th.pickle'))

###################################
#  Store testing generator labels  #
###################################

testing_generator = data_generators.get('testing')
y_true = np.concatenate([testing_generator[x][1][:, 1:]
                         for x in range(len(testing_generator))]).flatten()

pickle_file(y_true, path.join(
    batch_path, 'test_gen_32_full_seq_labels.pickle'))

##################
#  Create model  #
##################
print("Building model")

build_run_env(paths)

NNet = load_module(network_cfg.meta.architecture)

nn = NNet(
    network_cfg.meta.version,
    input_shape,
    paths,
    network_cfg.training_params,
    network_cfg.layers,
)

model = nn.build_from_config()
nn.compile(model)
nn.train(model, data_generators.get('training'),
         data_generators.get('validation'))

evaluation_data = nn.evaluate(model, data_generators.get('testing'), False)

#####################
#  Make prediction  #
#####################
print("Using model to make a prediction")
model_path = path.join(TEST_DATA, 'models')

model_pretrained = load_model(path.join(model_path, '4.24_CNN.hdf5'))
y_pred = model_pretrained.predict_generator(data_generators.get('testing'))

predictions_path = path.join(paths.dirs.predictions, '4.24.prediction.pickle')
pickle_file(y_pred, predictions_path)
