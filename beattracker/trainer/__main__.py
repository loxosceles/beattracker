#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import json
import traceback
from inspect import cleandoc
from pathlib import Path
import argparse
from dot_configs.dot_configs import Configurations

from beattracker.constants import STATIC_CFG
from beattracker.trainer.train import initiate_training
from beattracker.util.conf_helpers import build_path_object
from beattracker.log_helper.log import print_queue_error
from beattracker.util.helpers import (silent_create,
                                      remove_confirm,
                                      silent_copy,
                                      create_file_list,
                                      )


def add_args_to_configs(args, configs):
    #  FIXME: Turn this function into a pure function
    if configs._store.get("args") is None:
        configs.set("args")

    for k, v in args.items():
        configs.args.set(k, v)


def populate_run_list(done_list, run_configs_dir):
    run_list = create_file_list(run_configs_dir)
    run_list = [x for x in run_list if x not in done_list]
    run_list.sort()
    return run_list


def dispatch_queue_items(queue):
    cfg_path = queue.pop(0)
    configs = Configurations(cfg_path).get_configuration()

    try:
        if configs.args:
            paths = build_path_object(configs, configs.args)
            for p in paths.dirs.values():
                print("Silently creating {}".format(p))
                silent_create(p)

            silent_copy(cfg_path, paths.dirs.run_dir)
            done_list.append(configs)
            print("-" * 30)
            print("Run list: ", queue)
            print("Next is: ", cfg_path)
            print("-" * 30)
    except FileNotFoundError:
        print("Configuration file not found!")
        sys.exit()

    try:
        initiate_training(configs, paths, logs)
    except Exception as e:
        # No matter the exception, we want it to skip to the next run config
        message = print_queue_error(configs, e, traceback)
        print(message)
        traceback.print_exc()
        queue_logs = silent_create(paths.dirs.queue_logs)
        with open(Path(queue_logs, "queue_logs.txt"), "a+") as outfile:
            outfile.write(message)


logs = {}
st_cfg = Configurations(STATIC_CFG).get_configuration()

# Arguments
help_c = cleandoc(
    """
    Specifies configuration file.  Full path is required.
    """
)

help_s = cleandoc(
    """
    Switch to sandbox.
    """
)

help_o = cleandoc(
    """
    Overwrite data.
    Deletes results from last run, specifically the model, weights, logs,
    history, run_config, and state and write into this last run directory. Run
    number will not be incremented.
    """
)

help_r = cleandoc(
    """
    Restart last run using the same directory. Run number will not be
    incremented.
    """
)

help_l = cleandoc(
    """
    Limited dataset.
    Restricts the dataset to 20 files.
    """
)

parser = argparse.ArgumentParser(
    description="Train neural networks for musical onset detection.")

parser.add_argument("configuration", nargs="?", help=help_c)
parser.add_argument("-s", "--sandbox", action="store_true", help=help_s)
parser.add_argument("-o", "--overwrite-run-dir",
                    action="store_true", help=help_o)
parser.add_argument("-r", "--restart-run",
                    action="store_true", help=help_r)
parser.add_argument("-l", "--limit-files",
                    action="store_true", help=help_l)

args = vars(parser.parse_args())
print(args)

if args["configuration"] and Path(args["configuration"]).is_file():
    #  'configuration' is file
    try:
        cfg_path = args["configuration"]
        c = Configurations(cfg_path)
    except FileNotFoundError:
        print("Configuration file not found!")
        sys.exit()

    if c.validated is False:
        sys.exit(1)

    configs = c.get_configuration()

    #  args["restart_run"] = True  # should overwrite
    #  args["limit_files"] = False # should not overwrite
    try:
        if configs.args:
            overwrite_args = any(
                {k: v for k, v in configs.args.items() if v is True}
            )
            if overwrite_args:
                add_args_to_configs(configs.args, configs)
    except AttributeError:
        add_args_to_configs(configs.args, configs)

    # args -r
    if not configs.args["restart_run"]:
        configs.meta.set("sequence", "")

    paths = build_path_object(configs, configs.args)
    print(paths)

    for p in paths.dirs.values():
        silent_create(p)

    # arg -o
    if args["overwrite_run_dir"]:
        # remove all files under base path (including pickle)
        remove_confirm(paths.dirs.run_dir)

    print(configs)

    #  silent_copy(cfg_path, paths.dirs.run_dir)
    print("Storing results in: ", paths.dirs.run_dir)
    with open(Path(paths.dirs.run_dir, "cnn.json"), "w") as outfile:
        json.dump(configs._store, outfile)

    initiate_training(configs, paths, logs)

elif args["configuration"] is None:
    #  'configuration' is directory
    queue_dir = Path(st_cfg.dirs.data, st_cfg.dirs.queue)
    print(queue_dir)
    done_list = []

    queue = populate_run_list(done_list, queue_dir)
    print(queue)

    while queue:
        try:
            dispatch_queue_items(queue)
        except Exception as e:
            print(e)

        queue = populate_run_list(done_list, queue_dir)
else:
    print("Something went wrong!")
    sys.exit(1)
