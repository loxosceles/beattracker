#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from os import path, environ
import importlib
from pathlib import Path
from random import shuffle
from sklearn.model_selection import train_test_split
from keras.models import load_model

from beattracker.constants import ROOT_DIR
from beattracker.util.data_generator import DataGenerator
from beattracker.log_helper.log import write_logs, timestamp
from beattracker.util.helpers import pickle_file, unpickle_file

# Disable the warning, doesn't enable AVX/FMA
environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
#  tf.logging.set_verbosity(tf.logging.ERROR)


def check_module(module_name):
    """Checks if module can be imported without actually importing it."""

    full_path = path.join(
        ROOT_DIR, f"beattracker/networks/{module_name}.py")
    module_spec = importlib.util.spec_from_file_location(
        module_name, full_path)

    if module_spec is None:
        print("Module: {} not found".format(module_name))
        return None

    return module_spec


def import_module_from_spec(module_spec):
    """Import the module via the passed in module specification."""

    module = importlib.util.module_from_spec(module_spec)
    module_spec.loader.exec_module(module)
    return module


def load_module(arch):
    module_spec = check_module(arch)
    if module_spec:
        module = import_module_from_spec(module_spec)
    else:
        sys.exit(1)

    return getattr(module, arch.upper())


def balance_dataset(indices):
    trues = [k for k, v in indices.items() if v[2] is True]
    len_trues = len(trues)
    falses = [k for k, v in indices.items() if v[2] is False]
    len_falses = len(falses)
    excess = len_falses - len_trues
    shuffle(trues)
    falses = falses[: len_falses - excess]
    return trues + falses


def determine_input_shape(indices, key, architecture, chunk_size):
    fobj = unpickle_file(indices[0][0])
    if architecture == "cnn":
        dim = (chunk_size, fobj.log_filt_specs[key].shape[1]) + (1,)
    elif architecture == "rnn":
        dim = (chunk_size, fobj.log_filt_specs[key].shape[1])
    else:
        print("Architecture not defined. Exiting.")
        sys.exit()

    return dim


def split_dataset(index_file, val_size, test_size, shuffle=True, random_state=42):
    #  extract excerpt names, discard duplicates
    excerpts = list({v[0] for k, v in index_file.items()})

    # lists of excerpt names
    train_fns, test_fns = train_test_split(
        excerpts, test_size=test_size, shuffle=shuffle, random_state=random_state
    )
    train_fns, val_fns = train_test_split(
        train_fns, test_size=val_size, shuffle=shuffle, random_state=random_state
    )

    print("Training files: {}".format(len(train_fns)))
    print("Validation files: {}".format(len(val_fns)))
    print("Testing files: {}".format(len(test_fns)))

    train_index = {k: v for k, v in index_file.items() if v[0] in train_fns}
    test_index = {k: v for k, v in index_file.items() if v[0] in test_fns}
    val_index = {k: v for k, v in index_file.items() if v[0] in val_fns}

    return {
        "train_index": train_index,
        "val_index": val_index,
        "test_index": test_index,
    }


def filter_by_key(keys, indices):
    return {x: indices[x] for x in keys}


def filter_invalid_indices(indices, chunk_size):
    threshold = chunk_size // 2

    upward_list = list(
        filter(lambda x: indices[x][1] in range(threshold), indices))
    pivotals = list(filter(lambda x: indices[x][1] == 0, indices))[1:]
    pivotals.append(len(indices))
    downward_list = [
        item for sublist in pivotals for item in range(sublist - threshold, sublist)
    ]

    #  print(downward_list)
    valid_indices = indices.keys() - (upward_list + downward_list)
    #  print(valid_indices)
    return {x: indices[x] for x in valid_indices}


def initiate_training(configs, paths, logs):

    nnet = load_module(configs.meta.architecture)

    frame_size = configs.audio_params.frame_size

    print("Path to index file: ", paths.files.pickle_index)
    indices = unpickle_file(paths.files.pickle_index)

    input_shape = determine_input_shape(
        indices,
        frame_size,
        configs.meta.architecture,
        configs.training_samples.chunk_size,
    )
    print("Determine input shape: ", input_shape)

    configs.training_samples.set("input_shape", input_shape)

    indices = filter_invalid_indices(
        indices, configs.training_samples.chunk_size)

    mapping = split_dataset(
        indices, configs.data_partitions.val_size,
        configs.data_partitions.test_size)

    #  save data partitions
    pickle_file(mapping, paths.files.mappings)

    batch_size = configs.training_params.batch_size

    training_generator = DataGenerator(
        "train",
        mapping["train_index"],
        frame_size,
        batch_size,
        "softmax",
        configs.training_samples,
        input_shape,
    )
    validation_generator = DataGenerator(
        "val",
        mapping["val_index"],
        frame_size,
        batch_size,
        "softmax",
        configs.training_samples,
        input_shape,
    )
    testing_generator = DataGenerator(
        "test",
        mapping["test_index"],
        frame_size,
        batch_size,
        "softmax",
        configs.training_samples,
        input_shape,
    )

    nn = nnet(
        configs.meta.version,
        input_shape,
        paths,
        configs.training_params,
        configs.layers,
    )

    if configs.args.restart_run and Path(paths.files.model).exists():
        try:
            print("Loading previous model ({})".format(paths.files.model))
            model = load_model(
                str(Path(paths.dirs.run_dir, paths.files.model)))
        except FileNotFoundError:
            print("Model not found.")
            print("Initializing new model from config")
            model = nn.build_from_config()
    else:
        print("Initializing new model from config")
        model = nn.build_from_config()

    nn.print_architecture(model)

    logs["start_training"] = timestamp()

    nn.compile(model)
    nn.train(model, training_generator, validation_generator)

    logs["end_training"] = timestamp()

    evaluation_data = nn.evaluate(model, testing_generator, False)
    nn_logs = nn.compile_logs()

    print("Test loss: ", evaluation_data["loss"])
    print("Test accuracy: ", evaluation_data["acc"])
    nn.print_confusion_matrix(evaluation_data["cm"])
    logs["num_train_files"] = str(len(mapping["train_index"]))
    logs["num_val_files"] = str(len(mapping["val_index"]))
    logs["num_test_files"] = str(len(mapping["test_index"]))

    logs["test_loss"] = evaluation_data["loss"]
    logs["test_accuracy"] = evaluation_data["acc"]
    logs["confusion_matrix"] = evaluation_data["cm"]

    logs["chunk_size"] = configs.training_samples.chunk_size
    logs["padding"] = configs.training_samples.margin
    logs["frame_size"] = configs.audio_params.frame_size

    #  # report (pandas frame)
    report, clf_report = nn.print_report(model, testing_generator)
    print(report)
    logs["report"] = report
    logs["clf_rep_precision"] = clf_report[0]
    logs["clf_rep_recall"] = clf_report[1]
    logs["clf_rep_fbeta_score"] = clf_report[2]
    logs["clf_rep_support"] = clf_report[3]

    stringlist = []
    model.summary(print_fn=lambda x: stringlist.append(x))
    summary = "\n".join(stringlist)
    logs["summary"] = summary

    # merge preprocessing logs with model logs
    logs.update(nn_logs)
    logs_path = nn.get_file("logs")
    write_logs(logs, logs_path)
