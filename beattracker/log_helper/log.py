#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from inspect import cleandoc
import datetime


def timestamp():
    return str(datetime.datetime.now()).split(".")[0]


def write_logs(log_events, log_path):

    log_body = cleandoc(
        """
{separator}
{nn_architecture} loaded.
Version {version}

Start time training: {start_training}
End time training:   {end_training}

Training parameters:
--------------------
Input shape:        {input_shape}
Batch size:         {batch_size}
Epochs:             {epochs}
Metrics: 	        {metrics}
Optimizer:          {optimizer}
Losses:             {losses}
Training files:     {num_train_files}
Validation files:   {num_val_files}
Test Files: 	    {num_test_files}

Training data:
--------------
Chunk size:         {chunk_size}
Padding:            {padding}

Audio settings:
--------------
Frame size:         {frame_size}

Layers:
-------
{layers}

Summary:
--------
{summary}

Output from precision_recall_fscore_support
-------------------------------------------
PRFS precision: {clf_rep_precision}
PRFS recall: {clf_rep_recall}
PRFS fbeta score: {clf_rep_fbeta_score}
PRFS support: {clf_rep_support}

Confusion Matrix:
-----------------
{confusion_matrix}

Report:
-------
Test Loss:          {test_loss}
Test Accuracy:      {test_accuracy}
{report}

{separator}
""".format(
            separator=65 * "*", **log_events
        )
    )

    with open(log_path, 'w') as outfile:
        outfile.write(log_body)


def print_queue_error(error, cfg, traceback):
    return cleandoc(
        """
                \n+++++++++++ ABORTED ++++++++++
                {}
                {}
                {}
                ++++++++++++++++++++++++++++++\n
                """.format(
            cfg, error, traceback.format_exc()
        )
    )
