Feedback
========

If you have any suggestions or questions about **Beat Tracker** feel free to email me
at loxosceles@gmx.de.

If you encounter any errors or problems with **Beat Tracker**, please let me know!
Open an Issue at the GitLab http://gitlab.com/loxosceles/beattracker main repository.
