.. _getting_started:


***************
Getting started
***************

If you haven't already, follow the instructions in the :ref:`Installation`.

Next, you can get a prediction for a song on your local hard drive by issuing the following command:


.. code-block:: python

  python -m beattracker.tracker path/to/song.wav

