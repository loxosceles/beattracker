.. _installation:

============
Installation
============

First clone the repository::

  git clone https://gitlab.com/loxosceles/beattracker.git

Change into the root directory::

   cd beattracker

Make the :file:`install.sh` executable and run it::

   chmod 750 install.sh
   ./install.sh

The script will copy the default configuration to :file:`$HOME/.config/beattracker`. Then
it will ask where to find the data directory and the pickle cache directory. The
defaults should be fine in most cases. Howewer, if you want to move them to another
place on your hard drive, feel free to overwrite them.


======================
Downloading the models
======================

In order to use pretrained models you will have to download them first:

