.. include:: ../README.rst

Contents:
=========

.. toctree::
   :maxdepth: 2

   getting_started
   installation
   training
   feedback
   contributing
   history

