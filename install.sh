#!/usr/bin/env bash

# Constants
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CONFIGS_JSON=${SCRIPT_DIR%%/}/configs/conf.json
EXAMPLES_DIR=${SCRIPT_DIR%%/}/examples
TEST_DIR=${SCRIPT_DIR%%/}/test
DEFAULT_CONF_DIR="$HOME/.config/beattracker"
DEFAULT_DATA_DIR="$HOME/beattracking_data"

confirm_overwrite () {
  read confirm
  if [ $? -eq 0 ] && [ -z $confirm ] ||
     [ $confirm = "y" ] ||
     [ $confirm = "Y" ]; then
    return 0
  else
    return 1
  fi
}

enter_default () {
  if [ $? -eq "0" ] && [ -z $1 ]; then
    # [ENTER] and accept default
    echo $DEFAULT_DATA_DIR
  else
    echo $1
  fi
}

clean_path () {
  echo "${1/\~/$HOME}" | sed 's:/*$::'
}


echo "Installing beattracker"
echo "Press [ENTER] to confirm default values or specify new path."

test_dir=$TEST_DIR

conf_dir=$DEFAULT_CONF_DIR
full_path="${conf_dir}/conf.json"

if [ -f $full_path ]; then
  # config file exists: ask if we overwrite it
  echo -n "$full_path exists. Overwrite (Y/n): "

  if confirm_overwrite; then
    echo  "Overwriting old configuration file."
    cp $CONFIGS_JSON $conf_dir
  fi
else
  # config file does not exist: create path and copy example conf
  mkdir -p $conf_dir
  cp $CONFIGS_JSON $conf_dir
fi

read -e -p "Data directory: ($DEFAULT_DATA_DIR): " data_dir
read -e -p "Pickle directory: ($DEFAULT_DATA_DIR): " pickle_dir

# if [ENTER] pressed take default else take new value
data_dir=$(enter_default $data_dir)
pickle_dir=$(enter_default $pickle_dir)

# Clean paths
data_dir=$(clean_path $data_dir)
pickle_dir=$(clean_path $pickle_dir)

# Write new paths to config file
# insert confi dir into config file
sed -i "s|\(\"configs\": \).*$|\1\"$conf_dir\",|" $full_path
# insert result dir
sed -i "s|\(\"data\": \).*$|\1\"${data_dir}\",|" $full_path
# insert pickle dir
sed -i "s|\(\"pickle\": \).*$|\1\"${pickle_dir}/pickle\",|" $full_path
#  insert test dir
sed -i "s|\(\"test_env\": \).*$|\1\"${test_dir}\",|" $full_path

queue=${data_dir}/queue
echo "Queue : $queue"
mkdir -p ${queue}/examples
echo "Please put your datasets inside ${data_dir}/datasets"
mkdir ${data_dir}/datasets >/dev/null 2>&1
mkdir -p $pickle_dir

echo "Copying example networks"
cp -r $EXAMPLES_DIR/*.json ${queue}/examples

# pip install -r requirements.txt
